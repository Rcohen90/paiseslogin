import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComunicaService } from '../services/comunica.service';
import Swal from 'sweetalert2';
import { Valida } from '../interfaces/valida.interface';
import { Router } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formi: FormGroup;
  user: string;
  nameUser: string;

  constructor(private formBuilder: FormBuilder,
              private _comunicaService: ComunicaService,
              private _guard: AuthGuard,
              private route: Router) {

  }

  ngOnInit() {
    this.crearFormulario();
  }

  get emailNoValido(){
    return this.formi.get('email').invalid && this.formi.get('email').touched;
  }

  get passwordNoValido(){
    return this.formi.get('password').invalid && this.formi.get('password').touched;
  }

  crearFormulario(){
    this.formi = this.formBuilder.group({
      email  : ['', [ Validators.required, Validators.email] ],
      password: ['', Validators.required]
    });
  }

  iniciarSesion(){
    if(this.formi.invalid){
      return Object.values(this.formi.controls).forEach(control => {
        if(control instanceof FormGroup){
          Object.values(control.controls).forEach(control => control.markAsTouched());
        } else {
          control.markAsTouched();
        }
      })
    } else {
      this.validaAccount();
    }
  }

  validaAccount() {
    let request : Valida = {
       email : this.formi.value.email,
       password: this.formi.value.password
    }

    this.user = this.formi.value.email;

    let requestStr = JSON.stringify(request);

    this._comunicaService.validaAcount(requestStr)
      .subscribe((resp) => {
        this.validaResponse(resp, resp.status);
      }, response => {
          this.validaResponse(response, response.status);
        })
  }

  validaResponse(respo, estatus) {
    switch (estatus) {
      case 200:
        Swal.fire({
          title: 'Bienvenido',
          icon: 'success',
          timer: 600
        })
        this._guard.setIsAutenticado(true);
        this._comunicaService.setUser(this.user);
        this.route.navigate(['/paises']);
        break;
        default:
          Swal.fire({
            title: 'Error',
            text: 'Error al iniciar sesión',
            icon: 'error',
            timer: 1000
          })
          this._guard.setIsAutenticado(false);
        break;
    }
    this.formi.reset();
  }


}
