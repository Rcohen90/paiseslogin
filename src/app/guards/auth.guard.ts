import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { ComunicaService } from '../services/comunica.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  logueado: boolean = false;

  constructor(private router: Router) {

  }

  setIsAutenticado(log: boolean) {
    this.logueado = log;
  }

  getIsAutenticado(): boolean {
    return this.logueado;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if(this.getIsAutenticado()){
        return this.getIsAutenticado();
      }else{
        this.router.navigate(['/']);
        return this.getIsAutenticado();
      }
  }

}
