import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComunicaService } from '../services/comunica.service';
import Swal from 'sweetalert2';
import { Create } from '../interfaces/create.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  formi: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private _comunicaService: ComunicaService,
              private router: Router) {
    this.crearFormulario();
  }

  ngOnInit() {
  }

  get nombreNoValido(){
    return this.formi.get('nombre').invalid && this.formi.get('nombre').touched;
  }

  get correoNoValido(){
    return this.formi.get('correo').invalid && this.formi.get('correo').touched;
  }

  get pass1NoValido(){
    return this.formi.get('pass1').invalid && this.formi.get('pass1').touched;
  }

  get pass2NoValido(){
    const pass1 = this.formi.get('pass1').value;
    const pass2 = this.formi.get('pass2').value;
    return ( pass1 === pass2 ) ? false : true;
  }

  crearFormulario(){
    this.formi = this.formBuilder.group({
      nombre  : ['', [ Validators.required, Validators.minLength(5) ]  ],
      correo  : ['', [ Validators.required, Validators.email]],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
    });
  }

  guardar(){
    if(this.formi.invalid){
      return Object.values(this.formi.controls).forEach(control => {
        if(control instanceof FormGroup){
          Object.values(control.controls).forEach(control => control.markAsTouched());
        } else {
          control.markAsTouched();
        }
      })
    } else {
      this.createAccount();
    }
  }

  createAccount() {
    let request : Create = {
       email : this.formi.controls.correo.value,
       password: this.formi.controls.pass1.value,
       nombre: this.formi.controls.nombre.value
    }

    this._comunicaService.createAccount(request)
      .subscribe((resp) => {
        this.validaResponse(resp, resp.status);
      }, response => {
          this.validaResponse(response, response.status);
        });
  }

  validaResponse(respo, estatus) {
    switch (estatus) {
      case 200:
        Swal.fire({
          title: 'Correcto',
          text: 'Cuenta añadida correctamente',
          icon: 'success',
          timer: 2000
        })
        this.router.navigate(['/login']);
        break;
      default:
        Swal.fire({
          title: 'Error',
          text: 'Error al añadir cuenta.',
          icon: 'error',
          timer: 2000
        })
        break;
    }
    this.formi.reset();
  }

}
