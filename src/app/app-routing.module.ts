import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateAccountComponent } from './create-account/create-account.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { PaisComponent } from './pais/pais.component';
import { PaisesComponent } from './paises/paises.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'paises', component: PaisesComponent, canActivate: [AuthGuard] },
  { path: 'pais/:id', component: PaisComponent, canActivate: [AuthGuard] },
  { path: 'create-account', component: CreateAccountComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
