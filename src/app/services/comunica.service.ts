import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { Pais } from '../interfaces/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class ComunicaService {

  private url = 'https://dummyintegracion.herokuapp.com';
  public usuario: string;
  public usuarioName: string;

  constructor(private http: HttpClient) { }

  createAccount = (request:any) :Observable<any> => {
    const headers = {
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    return this.http.post(`${this.url}/createAccount`,JSON.stringify(request), headers).pipe(map(res=>res));
  }

  validaAcount = (request: string) : Observable<any> => {
    const headers = {
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    return this.http.post(`${ this.url }/validaAccount`, request, headers)
      .pipe(map(result => {
        return result;
      }));
  }

  getPaises(usuario: string, categoria: string) {
    const params = new HttpParams({
      fromObject: {
        user: usuario,
        categoria: categoria,
      }
    });
    return this.http.get(`${ this.url }/readAllData`, {params: params})
    .pipe(
      map( this.crearArreglo ),
      delay(0)
    );
  }

  private crearArreglo( paisObjeto: object ) {
    const paises: Pais[] = [];
    if(paisObjeto === null ) {
      return [];
    }
    Object.keys( paisObjeto ).forEach( key => {
      const pais: Pais = paisObjeto[key];
      //pais.id = key;
      paises.push( pais );
    });
    return paises;
  }

  getPais( body ) {
    return this.http.get(`${ this.url }/readData`, {params: body})
      .pipe(map(result => {
        return result;
      }));
  }

  crearPais( bodyData){
    return this.http.post(`${ this.url }/createData`, bodyData)
            .pipe(
              map( (resp: any) => {
                return resp;
              })
            );
  }

  actualizarPais( data ) {
    return this.http.put(`${ this.url }/updateData`, data);
  }

  borrarPais( id: string ) {
    const params = new HttpParams({
      fromObject: {
        id: id,
      }
    });
    return this.http.delete(`${ this.url }/deleteData`, {params: params});
  }

  borrarTodo( usuario: string, categoria: string  ) {
    const params = new HttpParams({
      fromObject: {
        user: usuario,
        categoria: categoria,
      }
    });
    return this.http.delete(`${ this.url }/deleteAllData`, {params: params});
  }

  setUser(user: string) {
    this.usuario = user;
  }

  getUser(): string {
    return this.usuario;
  }
}
