import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public guard: AuthGuard,
              private router: Router) { }

  ngOnInit(): void {
  }

  logout() {
    Swal.fire({
      title: 'Salir',
      text: `¿Está seguro que desea cerrar sesión?`,
      icon: 'warning',
      showConfirmButton: true,
      confirmButtonText: 'Sí',
      showCancelButton: true,
      cancelButtonText: 'No'
    }).then( resp => {
        if ( resp.value ) {
          this.router.navigate(['/login']);
          this.guard.setIsAutenticado(false);
        }
    });
  }

}
