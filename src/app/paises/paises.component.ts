import { Component, OnInit } from '@angular/core';
import { Pais } from '../interfaces/pais.interface'
import { ComunicaService } from '../services/comunica.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.css']
})
export class PaisesComponent implements OnInit {

  paises: Pais[] = [];
  cargando = true;
  usuario: string;

  constructor(private _comunicaService: ComunicaService) { }

  ngOnInit(): void {
    this.cargando = true;
    this.usuario = this._comunicaService.getUser();
    this._comunicaService.getPaises(this.usuario, 'paises')
      .subscribe( resp => {
        this.paises = resp;
        this.cargando = false;
      });
  }

  borrarPais(pais: Pais, i: number) {
    Swal.fire({
      title: 'Borrar País',
      text: `¿Está seguro que desea borrar a ${ pais.data.pais }?`,
      icon: 'warning',
      showConfirmButton: true,
      confirmButtonText: 'Sí',
      showCancelButton: true,
      cancelButtonText: 'No'
    }).then( resp => {
        if ( resp.value ) {
          this.paises.splice(i,1);
          this._comunicaService.borrarPais(pais.id)
          .subscribe((resp) => {
            this.validaResponse(resp, status);
          }, response => {
              this.validaResponse(response, response.status);
            });
        }
    });
  }

  borrarTodo() {
    Swal.fire({
      title: 'Borrar País',
      text: `¿Está seguro que desea borrar todos los datos?`,
      icon: 'warning',
      showConfirmButton: true,
      confirmButtonText: 'Sí',
      showCancelButton: true,
      cancelButtonText: 'No'
    }).then( resp => {
        if ( resp.value ) {
          this._comunicaService.borrarTodo(this.usuario, 'paises')
          .subscribe((resp) => {
            this.validaResponse(resp, status);
            this.paises = [];
          }, response => {
            this.validaResponse(response, response.status);
            this.paises = [];
            });
        }
    });
  }

  validaResponse(respo, estatus) {
    switch (estatus) {
      case 200:
        Swal.fire({
          title: 'Correcto',
          text: 'Operación concluida correctamente',
          icon: 'success',
          timer: 2000
        })
        break;
      default:
        Swal.fire({
          title: 'Error',
          text: 'Error al realizar operación',
          icon: 'error',
          timer: 2000
        })
        break;
    }
  }


}
