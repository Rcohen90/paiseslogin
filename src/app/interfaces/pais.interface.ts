import { PaisModel } from '../interfaces/paisModel.interface'

export class Pais {
  id?: string
  user?: string
  categoria?: string
  data: PaisModel
}
