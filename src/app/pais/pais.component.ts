import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pais } from '../interfaces/pais.interface';
import { ComunicaService } from '../services/comunica.service';

import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { PaisModel } from '../interfaces/paisModel.interface';

@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.css']
})
export class PaisComponent implements OnInit {

  pais: Pais = new Pais();
  paisData: PaisModel = new PaisModel();
  bandNuevo = true;
  usuario: string;
  identif: string;
  validPais: boolean;
  validDominio: boolean;
  validContinente: boolean;
  validPoblacion: boolean;

  constructor( private _comunicaService: ComunicaService,
               private route: ActivatedRoute,
               private router: Router) { }

  ngOnInit(): void {
    this.usuario = this._comunicaService.getUser();
    this.identif = this.route.snapshot.paramMap.get('id');
    let request = {
      id : this.identif,
      user : this.usuario,
   }
    if ( this.identif !== 'nuevo' ) {
      this.bandNuevo = false;
      this._comunicaService.getPais( request )
      .subscribe( (resp: Pais) => {
        this.pais = resp;
        this.paisData = resp.data;
      });
    }
  }

  guardarPais(f: NgForm) {
    this.validators(f);
    if ( f.invalid ) {
      return;
    }

    Swal.fire({
      title: 'Espere',
      text: 'Guardando información',
      icon: 'info',
      allowOutsideClick: false,
      timer: 1200
    })
    Swal.showLoading();

    let data: Pais = {
      id: this.identif,
      user: this.usuario,
      categoria: 'paises',
      data: {
        pais: f.form.value.pais,
        continente: f.form.value.continente,
        dominio: f.form.value.dominio,
        poblacion: f.form.value.poblacion
      }
    }

    if ( this.pais.id ) {
      this._comunicaService.actualizarPais( data )
      .subscribe((resp) => {
        this.validaResponse(resp, status);
      }, response => {
        this.validaResponse(response, response.status);
      });
    } else {
      this._comunicaService.crearPais( data )
        .subscribe((resp) => {
          this.validaResponse(resp, resp.status);
        }, response => {
            this.validaResponse(response, response.status);
          });
    }
  }

  validaResponse(respo, estatus) {
    switch (estatus) {
      case 200:
        Swal.fire({
          title: 'Correcto',
          text: 'Operación concluida correctamente',
          icon: 'success',
          timer: 2000
        });
        this.router.navigate(['/paises']);
        break;
      default:
        Swal.fire({
          title: 'Error',
          text: 'Error al realizar operación',
          icon: 'error',
          timer: 2000
        })
        break;
    }
  }

  validators(f: NgForm) {
    this.validPais = f.form.controls.pais.invalid;
    this.validDominio = f.form.controls.dominio.invalid;
    this.validContinente = f.form.controls.continente.invalid;
    this.validPoblacion = f.form.controls.poblacion.invalid;
  }
}
